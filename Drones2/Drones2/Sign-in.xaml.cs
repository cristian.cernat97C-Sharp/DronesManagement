﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
namespace Drones2
{
    /// <summary>
    /// Interaction logic for Sign_in.xaml
    /// </summary>
    public partial class Sign_in : MetroWindow
    {
        public Sign_in()
        {
            InitializeComponent();
        }
        private async void btnShowDialog_Click(object sender, EventArgs e)
        {
            await this.ShowMessageAsync("Welcome", "User!");
        }
    }
}
