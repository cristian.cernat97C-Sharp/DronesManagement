﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using System.Globalization;
using System.Net;
using Microsoft.Maps.MapControl.WPF;
using Microsoft.Maps.MapControl.WPF.Design;

namespace Drones2
{
    /// <summary>
    /// Interaction logic for Harta.xaml
    /// </summary>
    public partial class Harta :UserControl
    {

        public Harta()
        {
            InitializeComponent();
            
        }
        public void addNewBotanica()
        {
            MapPolygon polygonB = new MapPolygon();
            polygonB.Fill = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Colors.Blue);
        
            polygonB.Stroke = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Colors.Green);
            polygonB.StrokeThickness = 5;
            polygonB.Opacity = 0.4;
            polygonB.Locations = new LocationCollection() {
                                                             new Location(47.001563,28.842373),
                                                             new Location(46.986342,28.833961),
                                                             new Location(46.971819,28.853188),
                                                             new Location(46.981892,28.905716),
                                                             new Location(47.006480,28.867950),
            };
           
            myMap.Children.Add(polygonB);
            
        }

        public void addNewCiocana()
        {
            MapPolygon polygonC = new MapPolygon();
            polygonC.Fill = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Colors.Blue);

            polygonC.Stroke = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Colors.Green);
            polygonC.StrokeThickness = 5;
            polygonC.Opacity = 0.4;
            polygonC.Locations = new LocationCollection() {
                                                             new Location(46.981892,28.905716),
                                                             new Location(46.993134,28.915329),
                                                             new Location(47.065913,28.898163),
                                                             new Location(47.067783,28.882713),
                                                             new Location(47.006480,28.867950),
            };

            myMap.Children.Add(polygonC);
        }

        public void addNewRascani()
        {
            MapPolygon polygon = new MapPolygon();
            polygon.Fill = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Colors.Blue);

            polygon.Stroke = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Colors.Green);
            polygon.StrokeThickness = 5;
            polygon.Opacity = 0.4;
            polygon.Locations = new LocationCollection() {
                                                             new Location(47.067783,28.882713),
                                                             new Location(47.073746,28.837051),
                                                             new Location(47.052464,28.825893),
                                                             new Location(47.006480,28.867950),
                                                             
            };

            myMap.Children.Add(polygon);
        }
        public void addNewCentru()
        {
            MapPolygon polygon = new MapPolygon();
            polygon.Fill = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Colors.Blue);

            polygon.Stroke = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Colors.Green);
            polygon.StrokeThickness = 5;
            polygon.Opacity = 0.4;
            polygon.Locations = new LocationCollection() {
                                                             new Location(47.052464,28.825893),
                                                             new Location( 47.024855,28.813362),
                                                             new Location(47.001563,28.842373),
                                                             new Location(47.006480,28.867950),


            };

            myMap.Children.Add(polygon);
        }
        public void addNewTeleCentru()
        {
            MapPolygon polygon = new MapPolygon();
            polygon.Fill = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Colors.Blue);

            polygon.Stroke = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Colors.Green);
            polygon.StrokeThickness = 5;
            polygon.Opacity = 0.4;
            polygon.Locations = new LocationCollection() {
                                                             new Location(47.024855,28.813362),
                                                             new Location(47.011513,28.800488),
                                                             new Location(46.995709,28.799458),
                                                             new Location(46.986342,28.833961),
                                                              new Location(47.001563,28.842373),
            };

            myMap.Children.Add(polygon);
        }


        public void addNewBuiucani()
        {
            MapPolygon polygon = new MapPolygon();
            polygon.Fill = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Colors.Blue);

            polygon.Stroke = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Colors.Green);
            polygon.StrokeThickness = 5;
            polygon.Opacity = 0.4;
            polygon.Locations = new LocationCollection() {   new Location(47.052464,28.825893),
                                                             
                                                             new Location(47.024855,28.813362),
                                                             new Location(47.026844,28.772678),
                                                             new Location(47.044393,28.735085),
                                                             new Location(47.052464,28.825893),
                                                              
            };

            myMap.Children.Add(polygon);
        }
        public void addNewDurlesti()
        {
            MapPolygon polygon = new MapPolygon();
            polygon.Fill = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Colors.Blue);

            polygon.Stroke = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Colors.Green);
            polygon.StrokeThickness = 5;
            polygon.Opacity = 0.4;
            polygon.Locations = new LocationCollection() {   new Location(47.044393,28.735085),
                                                             new Location(47.016546,28.738518),
                                                             new Location(46.995709,28.799458),
                                                             new Location(47.011513,28.800488),
                                                             new Location(47.024855,28.813362),
                                                             new Location(47.052464,28.825893),
                                                             new Location(47.024855,28.813362),

            };

            myMap.Children.Add(polygon);
        }
        LocationConverter locConverter = new LocationConverter();
        public void SwitchMapViews()
        {
            InitializeComponent();
            //Set focus on the map
            myMap.Focus();
            // Displays the current latitude and longitude as the map animates.
            myMap.ViewChangeOnFrame += new EventHandler<MapEventArgs>(viewMap_ViewChangeOnFrame);
            // The default animation level: navigate between different map locations.
            //viewMap.AnimationLevel = AnimationLevel.Full;
        }

        private void viewMap_ViewChangeOnFrame(object sender, MapEventArgs e)
        {
            // Gets the map object that raised this event.
            Map map = sender as Map;
            // Determine if we have a valid map object.
            if (map != null)
            {
                // Gets the center of the current map view for this particular frame.
                Location mapCenter = map.Center;

                // Updates the latitude and longitude values, in real time,
                // as the map animates to the new location.
                txtLatitude.Text = string.Format(CultureInfo.InvariantCulture,
                  "{0:F5}", mapCenter.Latitude);
                txtLongitude.Text = string.Format(CultureInfo.InvariantCulture,
                    "{0:F5}", mapCenter.Longitude);
            }
        }
        private void Centru_Click(object sender, RoutedEventArgs e)
        {
            // Parse the information of the button's Tag property
            string[] tagInfo = ((Button)sender).Tag.ToString().Split(' ');
            Location center = (Location)locConverter.ConvertFrom(tagInfo[0]);
            double zoom = System.Convert.ToDouble(tagInfo[1]);

            // Set the map view
            myMap.SetView(center, zoom);
            addNewRascani();
        }
        private void Rascani_Click(object sender, RoutedEventArgs e)
        {
            // Parse the information of the button's Tag property
            string[] tagInfo = ((Button)sender).Tag.ToString().Split(' ');
            Location center = (Location)locConverter.ConvertFrom(tagInfo[0]);
            double zoom = System.Convert.ToDouble(tagInfo[1]);

            // Set the map view
            myMap.SetView(center, zoom);
            addNewRascani();
        }
       private void Botanica_Click(object sender, RoutedEventArgs e)
        {
            // Parse the information of the button's Tag property
            string[] tagInfo = ((Button)sender).Tag.ToString().Split(' ');
            Location center = (Location)locConverter.ConvertFrom(tagInfo[0]);
            double zoom = System.Convert.ToDouble(tagInfo[1]);

            // Set the map view
            myMap.SetView(center, zoom);

            addNewBotanica();
        }
        private void Ciocana_Click(object sender, RoutedEventArgs e)
        {
            // Parse the information of the button's Tag property
            string[] tagInfo = ((Button)sender).Tag.ToString().Split(' ');
            Location center = (Location)locConverter.ConvertFrom(tagInfo[0]);
            double zoom = System.Convert.ToDouble(tagInfo[1]);

            // Set the map view
            myMap.SetView(center, zoom);

            addNewCiocana();
        }
        private void TeleCentru_Click(object sender, RoutedEventArgs e)
        {
            // Parse the information of the button's Tag property
            string[] tagInfo = ((Button)sender).Tag.ToString().Split(' ');
            Location center = (Location)locConverter.ConvertFrom(tagInfo[0]);
            double zoom = System.Convert.ToDouble(tagInfo[1]);

            // Set the map view
            myMap.SetView(center, zoom);

            addNewTeleCentru();
        }
        private void Buiucani_Click(object sender, RoutedEventArgs e)
        {
            // Parse the information of the button's Tag property
            string[] tagInfo = ((Button)sender).Tag.ToString().Split(' ');
            Location center = (Location)locConverter.ConvertFrom(tagInfo[0]);
            double zoom = System.Convert.ToDouble(tagInfo[1]);

            // Set the map view
            myMap.SetView(center, zoom);

            addNewBuiucani();
        }
        private void Durlesti_Click(object sender, RoutedEventArgs e)
        {
            // Parse the information of the button's Tag property
            string[] tagInfo = ((Button)sender).Tag.ToString().Split(' ');
            Location center = (Location)locConverter.ConvertFrom(tagInfo[0]);
            double zoom = System.Convert.ToDouble(tagInfo[1]);

            // Set the map view
            myMap.SetView(center, zoom);

            addNewDurlesti();
        }
        private void AnimationLevel_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBoxItem cbi = (ComboBoxItem)(((ComboBox)sender).SelectedItem);
            string v = cbi.Content as string;
            if (!string.IsNullOrEmpty(v) && myMap != null)
            {
                AnimationLevel newLevel = (AnimationLevel)Enum.Parse(typeof(AnimationLevel), v, true);
                myMap.AnimationLevel = newLevel;
            }
        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            
        }
    }
    }


