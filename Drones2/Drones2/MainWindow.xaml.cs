﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using System.Globalization;
using Microsoft.Maps.MapControl.WPF;
using Microsoft.Maps.MapControl.WPF.Design;
namespace Drones2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow

    {
        public MainWindow()
        {

            InitializeComponent();

        }
        
        private void TabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            // ... Get TabControl reference.
            var item = sender as TabControl;
            // ... Set Title to selected tab header.
            var selected = item.SelectedItem as TabItem;
            this.Title = selected.Header.ToString();
        }

        public bool IsOp = false;


        public void Disable(int a)
        {
            if (a == 1) { }

        }
        public void ToggleFlyout(int index)
        {

            var thisflyout = this.Flyouts.Items[index] as Flyout;

            for (int i = 0; i <= 3; i++)
            {
                if (i == index) continue;
                var flyout = this.Flyouts.Items[i] as Flyout;
                if (flyout.IsOpen == true) { flyout.IsOpen = false; }

            }

            thisflyout.IsOpen = !thisflyout.IsOpen;
        }
       
        public void Add_Drone_click(object sender, RoutedEventArgs e)
        {

            ToggleFlyout(0);
            AddDroneFlyout ob = new AddDroneFlyout();
            ob.Curatenie();
        }

        public void Edit_Drone_click(object sender, RoutedEventArgs e)
        {

            ToggleFlyout(1);
        }

        public void Delete_Drone_click(object sender, RoutedEventArgs e)
        {

            ToggleFlyout(2);
        }

        public void Users_click(object sender, RoutedEventArgs e)
        {
            Engine n = new Engine();
            n.Show();
        }

        public void Traffic_click(object sender, RoutedEventArgs e)
        {
            Engine2 n = new Engine2();
            n.Show();
        }

        public void Collisions_click(object sender, RoutedEventArgs e)
        {
            ToggleFlyout(3);
        }
        

    }
}
