﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data.SqlClient;
using System.Data;
using System.ComponentModel;
namespace Drones2
{
    /// <summary>
    /// Interaction logic for Editeaza.xaml
    /// </summary>
    public partial class Editeaza : UserControl
    {
        bool exists = false;

        public Editeaza()
        {
            InitializeComponent();
            FillData();
            SetDatePicker();
        }

        private void Edit_Save(object sender, RoutedEventArgs e)
        {

            Class1.connection.Close();
            //Views.CustomFlyout ob = new CustomFlyout();

            bool chk_result = CheckFields();
            //string sex = ob.CheckSex();
            //string buget = ob.CheckBuget();


            if (chk_result == true)
            {
                Class1.connection.Open();

                using (SqlCommand cmd = new SqlCommand("SELECT COUNT(*) FROM [Proprietari] WHERE Nume_Prop = @Nume_Prop AND Prenume_Prop = @Prenume_Prop", Class1.connection))
                {
                    cmd.Parameters.AddWithValue("Nume_Prop", Nume.Text);
                    cmd.Parameters.AddWithValue("Prenume_Prop", Prenume.Text);
                    exists = (int)cmd.ExecuteScalar() > 0;
                }

                if (exists == false)
                {
                    MessageBox.Show("Nu e asa proprietar");
                }
                else
                {
                    using (SqlCommand cmd = new SqlCommand("UPDATE [Proprietari] SET Login1=@Login1, Password1=@Password1, Email=@Email, Nume_Prop=@Nume_Prop, Prenume_Prop=@Prenume_Prop, Gen_Prop=@Gen_Prop, DatNas_Prop=@DatNas_Prop,Adresa_Prop=@Adresa_Prop, Tel_Prop=@Tel_Prop WHERE Nume_Prop = @Nume_Prop AND Prenume_Prop = @Prenume_Prop ", Class1.connection))
                    {
                        cmd.Parameters.AddWithValue("Login1", Login.Text);
                        cmd.Parameters.AddWithValue("Password1", Parola.Password);
                        cmd.Parameters.AddWithValue("Email", Email.Text);
                        cmd.Parameters.AddWithValue("Nume_Prop", Nume.Text);
                        cmd.Parameters.AddWithValue("Prenume_Prop", Prenume.Text);
                        cmd.Parameters.AddWithValue("Gen_Prop", CheckSex());
                        cmd.Parameters.AddWithValue("DatNas_Prop", DataNastere.SelectedDate.Value.Date.ToString("yyyy-MM-dd"));
                        cmd.Parameters.AddWithValue("Adresa_Prop", Adresa.Text);
                        cmd.Parameters.AddWithValue("Tel_Prop", Telefonul.Text);

                        cmd.ExecuteNonQuery();

                        MessageBox.Show("DONE");
                        Class1.connection.Close();
                    }
                }

                
                    Class1.connection.Close();
                
            }
            
        }
        private string CheckSex()
        {
            string gen = "";
            if (SexFem.IsChecked == true) gen = "F";
            else gen = "M";

            return gen;
        }
        void FillData()
        {
            string connectionString = @"Data Source=.\BLUECC;Initial Catalog=Drones;Integrated Security=True";
            SqlConnection connection = new SqlConnection(connectionString);


            using (connection)
            {
                connection.Open();

                using (SqlDataAdapter dataAdapter = new SqlDataAdapter("SELECT * FROM [Proprietari]", connection))
                {

                    DataTable dt = new DataTable();
                    dataAdapter.Fill(dt);

                    PropGrid.ItemsSource = dt.DefaultView;

                }
            }
        }

        public void SetDatePicker()
        {
            DataNastere.SelectedDate = new DateTime(2000, 1, 1);
            DataNastere.DisplayDateStart = new DateTime(1970, 1, 1);
            DataNastere.DisplayDateEnd = new DateTime(2005, 1, 1);
            DataNastere.DisplayDate = new DateTime(2000, 1, 1);
        }

      

             private bool CheckFields()
        {
            bool ok = true;

            if (Login.Text == "")
            {
                ok = false;
                Login.BorderBrush = new SolidColorBrush(Colors.Red);
            }
            else
            if (Parola.Password == "")
            {
                ok = false;
                Parola.BorderBrush = new SolidColorBrush(Colors.Red);
            }
            else
            if (Email.Text == "")
            {
                ok = false;
                Email.BorderBrush = new SolidColorBrush(Colors.Red);
            }
            else
            if (Nume.Text == "")
            {
                ok = false;
                Nume.BorderBrush = new SolidColorBrush(Colors.Red);
            }
            else if (Prenume.Text == "")
            {
                ok = false;
                Prenume.BorderBrush = new SolidColorBrush(Colors.Red);
            }

            else if (DataNastere.Text == "")
            {
                ok = false;
                DataNastere.BorderBrush = new SolidColorBrush(Colors.Red);
            }
            else if (Adresa.Text == "")
            {
                ok = false;
                Adresa.BorderBrush = new SolidColorBrush(Colors.Red);
            }
            else if (Telefonul.Text == "")
            {
                ok = false;
                Telefonul.BorderBrush = new SolidColorBrush(Colors.Red);
            }


            return ok;
        }
    }
}
