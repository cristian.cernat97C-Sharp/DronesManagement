﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data.SqlClient;
using System.Data;
using System.ComponentModel;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;

namespace Drones2
{
    /// <summary>
    /// Interaction logic for EditElevFlyout.xaml
    /// </summary>
    /// 
    public partial class EditElevFlyout : UserControl
    {
        bool exists = false;

        public EditElevFlyout()
        {
            InitializeComponent();
            FillData();
            
        }

        private async void Edit_Save(object sender, RoutedEventArgs e)
        {
            string connectionString = @"Data Source=.\BLUECC;Initial Catalog=Drones;Integrated Security=True";
            SqlConnection connection = new SqlConnection(connectionString);


            bool chk_result = CheckFields();
         


            if (chk_result == true)
            {
                connection.Open();

                using (SqlCommand cmd = new SqlCommand("SELECT COUNT(*) FROM [Drone] WHERE Nume_Dron = @Nume_Dron ", connection))
                {
                    cmd.Parameters.AddWithValue("Nume_Dron", NumeDron.Text);
                    exists = (int)cmd.ExecuteScalar() > 0;
                }

                if (exists == false)
                {
                    MessageBox.Show("Nu există așa dronă");
                }
                else
                {
                    using (SqlCommand cmd = new SqlCommand("UPDATE [Drone] SET Nume_Dron = @Nume_Dron, Audio_Dron = @Audio_Dron, Platform_Dron = @Platform_Dron, Zbor_Dron=@Zbor_Dron, General_Dron = @General_Dron, Putere_Dron = @Putere_Dron, Memorie_Dron = @Memorie_Dron WHERE Nume_Dron = @Nume_Dron", connection))
                    {
                        cmd.Parameters.AddWithValue("Nume_Dron", NumeDron.Text);
                        cmd.Parameters.AddWithValue("Audio_Dron", CheckAudio());
                        cmd.Parameters.AddWithValue("Platform_ron", Frecventa.Text);
                        cmd.Parameters.AddWithValue("Zbor_Dron", Zbor.Text);
                        cmd.Parameters.AddWithValue("General_Dron", Baterie.Text);
                        cmd.Parameters.AddWithValue("Putere_Dron", Numarelici.Text);
                        cmd.Parameters.AddWithValue("Memorie_Dron", Memorie.Text);
                        

                        cmd.ExecuteNonQuery();

                        var metroWindow = (Application.Current.MainWindow as MetroWindow);
                        await metroWindow.ShowMessageAsync("Operațiune", "Cu succes");

                        connection.Close();
                    }
                }

                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
        }

        void FillData()
        {
            string connectionString = @"Data Source=.\BLUECC;Initial Catalog=Drones;Integrated Security=True";
            SqlConnection connection = new SqlConnection(connectionString);

            using (connection)
            {
                connection.Open();

                using (SqlDataAdapter dataAdapter = new SqlDataAdapter("SELECT * FROM [Drone]", connection))
                {

                    DataTable dt = new DataTable();
                    dataAdapter.Fill(dt);

                    DronesGrid.ItemsSource = dt.DefaultView;

                }
            }
        }

        private void NumeElev_TextChanged(object sender, TextChangedEventArgs e)
        {

        }
        
        public string CheckAudio()
        {
            string audio = "";
            if (Audio.IsChecked == true) audio = "Da";
            else audio = "Nu";

            return audio;
        }


        public bool CheckFields()
        {
            bool ok = true;


            if (NumeDron.Text == "")
            {
                ok = false;
                NumeDron.BorderBrush = new SolidColorBrush(Colors.Red);
            }
            else if (Camera.Text == "")
            {
                ok = false;
                Camera.BorderBrush = new SolidColorBrush(Colors.Red);
            }
            else if (Frecventa.Text == "")
            {
                ok = false;
                Frecventa.BorderBrush = new SolidColorBrush(Colors.Red);
            }
            else if (Zbor.Text == "")
            {
                ok = false;
                Zbor.BorderBrush = new SolidColorBrush(Colors.Red);
            }
            else if (Baterie.Text == "")
            {
                ok = false;
                Baterie.BorderBrush = new SolidColorBrush(Colors.Red);
            }
            else if (Numarelici.Text == "")
            {
                ok = false;
                Numarelici.BorderBrush = new SolidColorBrush(Colors.Red);
            }
            else if (Baterie.Text == "")
            {
                ok = false;
                Baterie.BorderBrush = new SolidColorBrush(Colors.Red);
            }
            else if (Memorie.Text == "")
            {
                ok = false;
                Memorie.BorderBrush = new SolidColorBrush(Colors.Red);
            }
            
            return ok;
        }
    }
}


