--1
SELECT        dbo.Sectoare.Nume_Sec, dbo.Accidente.Nume_Acc, dbo.Proprietari.Nume_Prop, dbo.Proprietari.Prenume_Prop
FROM            dbo.Accidente INNER JOIN
                         dbo.Trafic ON dbo.Accidente.ID_Traf = dbo.Trafic.ID_Traf INNER JOIN
                         dbo.Sectoare ON dbo.Trafic.ID_Sec = dbo.Sectoare.ID_Sec INNER JOIN
                         dbo.Drone ON dbo.Trafic.ID_Traf = dbo.Drone.ID_Traf INNER JOIN
                         dbo.Proprietari ON dbo.Drone.ID_Prop = dbo.Proprietari.ID_Prop
GROUP BY dbo.Sectoare.Nume_Sec, dbo.Accidente.Nume_Acc, dbo.Proprietari.Nume_Prop, dbo.Proprietari.Prenume_Prop


--2
SELECT        dbo.Trafic.Nume_Traf, dbo.Sectoare.Nume_Sec, dbo.Proprietari.Nume_Prop, dbo.Proprietari.Prenume_Prop
FROM            dbo.Accidente INNER JOIN
                         dbo.Trafic ON dbo.Accidente.ID_Traf = dbo.Trafic.ID_Traf INNER JOIN
                         dbo.Sectoare ON dbo.Trafic.ID_Sec = dbo.Sectoare.ID_Sec INNER JOIN
                         dbo.Drone ON dbo.Trafic.ID_Traf = dbo.Drone.ID_Traf INNER JOIN
                         dbo.Proprietari ON dbo.Drone.ID_Prop = dbo.Proprietari.ID_Prop
GROUP BY dbo.Sectoare.Nume_Sec, dbo.Proprietari.Nume_Prop, dbo.Proprietari.Prenume_Prop, dbo.Trafic.Nume_Traf

--3
SELECT        dbo.Drone.Nume_Dron, dbo.Trafic.Nume_Traf, dbo.Sectoare.Nume_Sec, dbo.Proprietari.Nume_Prop
FROM            dbo.Trafic INNER JOIN
                         dbo.Sectoare ON dbo.Trafic.ID_Sec = dbo.Sectoare.ID_Sec INNER JOIN
                         dbo.Drone ON dbo.Trafic.ID_Traf = dbo.Drone.ID_Traf INNER JOIN
                         dbo.Proprietari ON dbo.Drone.ID_Prop = dbo.Proprietari.ID_Prop
GROUP BY dbo.Sectoare.Nume_Sec, dbo.Proprietari.Nume_Prop, dbo.Trafic.Nume_Traf, dbo.Drone.Nume_Dron
--4
SELECT        dbo.Drone.Nume_Dron, dbo.Trafic.Nume_Traf, dbo.Sectoare.Nume_Sec, dbo.Clase.Nume_Clasa
FROM            dbo.Trafic INNER JOIN
                         dbo.Sectoare ON dbo.Trafic.ID_Sec = dbo.Sectoare.ID_Sec INNER JOIN
                         dbo.Drone ON dbo.Trafic.ID_Traf = dbo.Drone.ID_Traf INNER JOIN
                         dbo.Proprietari ON dbo.Drone.ID_Prop = dbo.Proprietari.ID_Prop INNER JOIN
                         dbo.Clase ON dbo.Drone.ID_Clasa = dbo.Clase.ID_Clasa
						 where Gen_Prop='M'
GROUP BY dbo.Sectoare.Nume_Sec, dbo.Trafic.Nume_Traf, dbo.Drone.Nume_Dron, dbo.Clase.Nume_Clasa



--5
SELECT        dbo.Drone.Nume_Dron, dbo.Sectoare.Nume_Sec, dbo.Proprietari.DatNas_Prop, dbo.Trafic.Permis_Traf
FROM            dbo.Trafic INNER JOIN
                         dbo.Sectoare ON dbo.Trafic.ID_Sec = dbo.Sectoare.ID_Sec INNER JOIN
                         dbo.Drone ON dbo.Trafic.ID_Traf = dbo.Drone.ID_Traf INNER JOIN
                         dbo.Proprietari ON dbo.Drone.ID_Prop = dbo.Proprietari.ID_Prop
						 where DatNas_Prop between '01.01.1997' and '01.07.1997' 
GROUP BY dbo.Sectoare.Nume_Sec, dbo.Drone.Nume_Dron, dbo.Proprietari.DatNas_Prop, dbo.Trafic.Permis_Traf


--6
SELECT        dbo.Sectoare.Nume_Sec, dbo.Trafic.Nume_Traf, COUNT(dbo.Accidente.ID_Acc) AS [Total Accidente]
FROM            dbo.Trafic INNER JOIN
                         dbo.Sectoare ON dbo.Trafic.ID_Sec = dbo.Sectoare.ID_Sec INNER JOIN
                         dbo.Drone ON dbo.Trafic.ID_Traf = dbo.Drone.ID_Traf INNER JOIN
                         dbo.Clase ON dbo.Drone.ID_Clasa = dbo.Clase.ID_Clasa INNER JOIN
                         dbo.Accidente ON dbo.Trafic.ID_Traf = dbo.Accidente.ID_Traf
GROUP BY dbo.Sectoare.Nume_Sec, dbo.Trafic.Nume_Traf

--7

SELECT        dbo.Accidente.Nume_Acc, dbo.Clase.Nume_Clasa, dbo.Drone.Nume_Dron,Data_Acc
FROM            dbo.Drone INNER JOIN
                         dbo.Clase ON dbo.Drone.ID_Clasa = dbo.Clase.ID_Clasa INNER JOIN
                         dbo.Trafic ON dbo.Drone.ID_Traf = dbo.Trafic.ID_Traf INNER JOIN
                         dbo.Accidente ON dbo.Trafic.ID_Traf = dbo.Accidente.ID_Traf
						 
GROUP BY dbo.Accidente.Nume_Acc, dbo.Clase.Nume_Clasa, dbo.Drone.Nume_Dron,Data_Acc

--8
SELECT       dbo.Accidente.Nume_Acc, dbo.Clase.Nume_Clasa, dbo.Drone.Nume_Dron,dbo.Accidente.Data_Acc
FROM            dbo.Drone INNER JOIN
                         dbo.Clase ON dbo.Drone.ID_Clasa = dbo.Clase.ID_Clasa INNER JOIN
                         dbo.Trafic ON dbo.Drone.ID_Traf = dbo.Trafic.ID_Traf INNER JOIN
                         dbo.Accidente ON dbo.Trafic.ID_Traf = dbo.Accidente.ID_Traf
						 where Data_Acc between '01.feb.2016' and '26.apr.2016'
GROUP BY dbo.Accidente.Nume_Acc, dbo.Clase.Nume_Clasa, dbo.Drone.Nume_Dron,dbo.Accidente.Data_Acc
--9
SELECT        dbo.Proprietari.Nume_Prop, dbo.Proprietari.Prenume_Prop, COUNT(dbo.Accidente.ID_Acc) AS [Total accidente]
FROM            dbo.Drone INNER JOIN
                         dbo.Clase ON dbo.Drone.ID_Clasa = dbo.Clase.ID_Clasa INNER JOIN
                         dbo.Proprietari ON dbo.Drone.ID_Prop = dbo.Proprietari.ID_Prop INNER JOIN
                         dbo.Trafic ON dbo.Drone.ID_Traf = dbo.Trafic.ID_Traf INNER JOIN
                         dbo.Accidente ON dbo.Trafic.ID_Traf = dbo.Accidente.ID_Traf INNER JOIN
                         dbo.Sectoare ON dbo.Trafic.ID_Sec = dbo.Sectoare.ID_Sec
GROUP BY dbo.Proprietari.Nume_Prop, dbo.Proprietari.Prenume_Prop


--10
SELECT        dbo.Clase.Nume_Clasa, dbo.Sectoare.Nume_Sec, dbo.Sectoare.GradPericol_Sec
FROM            dbo.Drone INNER JOIN
                         dbo.Clase ON dbo.Drone.ID_Clasa = dbo.Clase.ID_Clasa INNER JOIN
                         dbo.Trafic ON dbo.Drone.ID_Traf = dbo.Trafic.ID_Traf INNER JOIN
                         dbo.Sectoare ON dbo.Trafic.ID_Sec = dbo.Sectoare.ID_Sec
GROUP BY dbo.Clase.Nume_Clasa, dbo.Sectoare.Nume_Sec, dbo.Sectoare.GradPericol_Sec