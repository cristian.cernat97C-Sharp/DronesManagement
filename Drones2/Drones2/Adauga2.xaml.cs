﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using System.Data.SqlClient;
using System.Data;
namespace Drones2
{
    /// <summary>
    /// Interaction logic for Adauga.xaml
    /// </summary>
    public partial class Adauga2 : UserControl
    {
        public Adauga2()
        {
            InitializeComponent();
        }
        String sqlStatement = "SELECT TOP 1 ID_Traf FROM [Trafic] ORDER BY ID_Traf DESC";
        bool exists = false;
        int studCounter = 0;



        public void Curatenie()
        {
            Nume_Traf.Clear();
            PermisTraf.Clear();
        }

        private int SectorStuff()
        {
            int Sec = 1;
            string connectionString = @"Data Source=.\BLUECC;Initial Catalog=Drones;Integrated Security=True";
            SqlConnection connection = new SqlConnection(connectionString);

            string local = "SELECT ID_Sec FROM [Sectoare] WHERE Nume_Sec = @Nume_Sec AND GradPericol_Sec=@GradPericol_Sec ";
            SqlCommand cmd = new SqlCommand(local, connection);
            connection.Open();
            cmd.Parameters.AddWithValue("@Nume_Sec", NumeSector.Text);
            cmd.Parameters.AddWithValue("@GradPericol_Sec", GradPericol.Text);
            try
            {
                Sec = (int)(cmd.ExecuteScalar());
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }

            connection.Close();
            return Sec;
        }
        private async void Create_Traffic(object sender, RoutedEventArgs e)
        {

            bool chk_result = CheckFields();

            if (chk_result == true)
            {
                Class1.connection.Open();

                using (SqlCommand cmdCount = new SqlCommand(sqlStatement, Class1.connection))
                {
                    studCounter = (int)cmdCount.ExecuteScalar() + 1;
                }

                using (SqlCommand cmd = new SqlCommand("SELECT COUNT(*) FROM [Trafic] WHERE Nume_Traf = @Nume_Traf", Class1.connection))
                {
                    cmd.Parameters.AddWithValue("Nume_Traf", Nume_Traf.Text);
                    exists = (int)cmd.ExecuteScalar() > 0;
                }

                if (exists)
                {
                    var metroWindow = (Application.Current.MainWindow as MetroWindow);
                    await metroWindow.ShowMessageAsync("Exista", "Asa Trafic");
                }
                else
                {
                    using (SqlCommand cmd = new SqlCommand("INSERT INTO [Trafic] VALUES (@ID_Traf,@Nume_Traf @ID_Sec, @Permis_Traf)", Class1.connection))
                    {
                        cmd.Parameters.AddWithValue("ID_Traf", studCounter);
                        cmd.Parameters.AddWithValue("Nume_Traf", Nume_Traf.Text);
                        cmd.Parameters.AddWithValue("ID_Sec", SectorStuff());
                        cmd.Parameters.AddWithValue("Permis_Traf", PermisTraf.Text);

                        cmd.ExecuteNonQuery();

                        var metroWindow = (Application.Current.MainWindow as MetroWindow);
                        await metroWindow.ShowMessageAsync("Adăugare", "Succes");
                        Curatenie();
                        Class1.connection.Close();

                    }

                }

                if (Class1.connection.State == ConnectionState.Open)
                {
                    Class1.connection.Close();
                }
            }
        }

     
        private bool CheckFields()
        {
            bool ok = true;

            if (Nume_Traf.Text == "")
            {
                ok = false;
                Nume_Traf.BorderBrush = new SolidColorBrush(Colors.Red);
            }
           
            else
            if (NumeSector.Text == "")
            {
                ok = false;
                NumeSector.BorderBrush = new SolidColorBrush(Colors.Red);
            }
            else
            if (GradPericol.Text == "")
            {
                ok = false;
                GradPericol.BorderBrush = new SolidColorBrush(Colors.Red);
            }
            else if (PermisTraf.Text == "")
            {
                ok = false;
                PermisTraf.BorderBrush = new SolidColorBrush(Colors.Red);
            }

            return ok;
        }



    }

}
