﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data.SqlClient;
using MahApps.Metro.Controls.Dialogs;
using System.Data;
using MahApps.Metro.Controls;

namespace Drones2
{
    /// <summary>
    /// Interaction logic for CustomFlyout.xaml
    /// </summary>
    public partial class AddDroneFlyout : UserControl
    {
        String sqlStatement = "SELECT TOP 1 ID_Dron FROM [Drone] ORDER BY ID_Dron DESC";
        bool exists = false;
        int studCounter = 0;

        public AddDroneFlyout()
        {
            InitializeComponent();
          
            
        }

        public void Curatenie()
        {
            NumeDron.Clear();
            Frecventa.Clear();
            Memorie.Clear();
            Camera.Clear();
            Baterie.Clear();
            Zbor.Clear();
            Numarelici.Clear();
            Audio.IsChecked = false;
        }


        private async void Create_Drone(object sender, RoutedEventArgs e)
        {
            string connectionString = @"Data Source=.\BLUECC;Initial Catalog=Drones;Integrated Security=True";
            SqlConnection connection = new SqlConnection(connectionString);

           
            bool chk_result = CheckFields();

            if (chk_result == true)
            {
                connection.Open();

                using (SqlCommand cmdCount = new SqlCommand(sqlStatement, connection))
                {
                    studCounter = (int)cmdCount.ExecuteScalar() + 1;
                }

                using (SqlCommand cmd = new SqlCommand("SELECT COUNT(*) FROM [Drone] WHERE Nume_Dron=@Nume_Dron" , connection))
                {
                    cmd.Parameters.AddWithValue("Nume_Dron", NumeDron.Text);
                    exists = (int)cmd.ExecuteScalar() > 0;
                }

                if (exists)
                {
                    MessageBox.Show("Student already exists!");
                }
                else
                {
                    using (SqlCommand cmd = new SqlCommand("INSERT INTO [Drone] VALUES (@ID_Dron, @Nume_Dron, @Audio_Dron, @Camera_Dron, @Platform_Dron, @Zbor_Dron, @General_Dron, @Putere_Dron, @Memorie_Dron)", connection))
                    {
                        cmd.Parameters.AddWithValue("ID_Dron", studCounter);
                        cmd.Parameters.AddWithValue("Nume_Dron", NumeDron.Text);
                        cmd.Parameters.AddWithValue("Audio_Dron", CheckAudio());
                        cmd.Parameters.AddWithValue("Camera_Dron", Camera.Text);
                        cmd.Parameters.AddWithValue("Platforma_Dron", Frecventa);
                        cmd.Parameters.AddWithValue("Zbor_Dron", Zbor.Text);
                        cmd.Parameters.AddWithValue("General_Dron", Baterie.Text);
                        cmd.Parameters.AddWithValue("Putere_Dron", Numarelici.Text);
                        cmd.Parameters.AddWithValue("Memorie", Memorie);
                       

                        cmd.ExecuteNonQuery();

                        var metroWindow = (Application.Current.MainWindow as MetroWindow);
                        await metroWindow.ShowMessageAsync("Operațiune", "Cu succes");

                        connection.Close();
                    }
                }

                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
        }

    

        private string CheckAudio()
        {
            string audio = "";
            if (Audio.IsChecked == true) audio = "Da";
            else audio = "NU";

            return audio;
        }

        private bool CheckFields()
        {
            bool ok = true;


            if (NumeDron.Text == "")
            {
                ok = false;
                NumeDron.BorderBrush = new SolidColorBrush(Colors.Red); 
            }
            else if (Camera.Text == "")
            {
                ok = false;
                Camera.BorderBrush = new SolidColorBrush(Colors.Red);
            }
            else if (Frecventa.Text == "")
            {
                ok = false;
                Frecventa.BorderBrush = new SolidColorBrush(Colors.Red);
            }
            else if (Zbor.Text == "")
            {
                ok = false;
                Zbor.BorderBrush = new SolidColorBrush(Colors.Red);
            }
            else if (Baterie.Text == "")
            {
                ok = false;
                Baterie.BorderBrush = new SolidColorBrush(Colors.Red);
            }
            else if (Numarelici.Text == "")
            {
                ok = false;
                Numarelici.BorderBrush = new SolidColorBrush(Colors.Red);
            }
            else if (Memorie.Text == "")
            {
                ok = false;
                Memorie.BorderBrush = new SolidColorBrush(Colors.Red);
            }

            return ok;
        }


    }
}
