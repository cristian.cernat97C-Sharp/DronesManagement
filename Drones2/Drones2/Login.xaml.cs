﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data.SqlClient;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using System.Data;

namespace Drones2
{
    /// <summary>
    /// Interaction logic for CustomFlyout.xaml
    /// </summary>
    public partial class LoginFlyout : UserControl
    {
        
        
        bool login12 = false;
        bool pass12 = false;

       
        public LoginFlyout()
        {
            InitializeComponent();
   
        }

        public void Curatenie()
        {
            Login.Clear();
            Parola.Clear();
            
        }


        private async void Login_click(object sender, RoutedEventArgs e)
        {
            String log1 = "SELECT Login1 FROM [Proprietari] WHERE Login1='" + Login.Text + "'";
            String pas1 = "SELECT Password1 FROM [Proprietari]WHERE Password1='" + Parola.Password + "'";

            bool chk_result = CheckFields();

            if (chk_result == true)
            {
                try {
                    Class1.connection.Open();

                    SqlCommand cmd1 = new SqlCommand(log1, Class1.connection);
                    SqlDataReader dr1 = cmd1.ExecuteReader();

                    while (dr1.Read())
                    {
                        login12 = true;
                    }
                    dr1.Close();

                    if (login12 == true)
                    {

                        SqlCommand cmd2 = new SqlCommand(pas1, Class1.connection);
                        SqlDataReader dr2 = cmd2.ExecuteReader();
                        while (dr2.Read())
                        {
                            pass12 = true;
                        }
                        dr2.Close();
                        if (pass12 == true)
                        {

                            var metroWindow = (Application.Current.MainWindow as MetroWindow);
                            await metroWindow.ShowMessageAsync("Succes", "Logare...");
                            Curatenie();
                            Welcome n = new Welcome();

                            MainWindow w = new MainWindow();
                            w.ShowDialog();
                            n.ToggleFlyout(1);




                        }
                        else
                        {
                            var metroWindow = (Application.Current.MainWindow as MetroWindow);
                            await metroWindow.ShowMessageAsync("Eroare", "Parola incorectă");
                            Curatenie();

                        }


                    }
                    else
                    {
                        var metroWindow = (Application.Current.MainWindow as MetroWindow);
                        await metroWindow.ShowMessageAsync("Eroare", "Login incorect");

                    }



                    if (Class1.connection.State == ConnectionState.Open)
                    {
                        Class1.connection.Close();
                    }
                }
                catch (Exception ex)
                {
                    var metroWindow = (Application.Current.MainWindow as MetroWindow);
                    await metroWindow.ShowMessageAsync("Eroare", ex.Message);
                }
            }
           

        }


        private bool CheckFields()
        {
            bool ok = true;

            if (Login.Text == "")
            {
                ok = false;
                Login.BorderBrush = new SolidColorBrush(Colors.Red);
            }else
            if (Parola.Password == "")
            {
                ok = false;
                Parola.BorderBrush = new SolidColorBrush(Colors.Red);
            }
            
            return ok;
        }

   
      
    }
}
