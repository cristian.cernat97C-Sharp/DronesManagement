﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data.SqlClient;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using System.Data;
namespace Drones2
{
    /// <summary>
    /// Interaction logic for Sterge.xaml
    /// </summary>
    public partial class Stergere2 : UserControl
    {
        public Stergere2()
        {
            InitializeComponent();
        }


        public void Curatenie()
        {
            Nume_Traf.Clear();
            Nume_Traf.Clear();
            GradPericol.Clear();
            PermisTraf.Clear();
        }

        private async void Sterge2_click(object sender, RoutedEventArgs e)
        {
            bool chk_result = CheckFields();

            if (chk_result == true)
            {
                SqlConnection connection2 = new SqlConnection(@"Data Source = .\BLUECC; Initial Catalog = Drones; Integrated Security=True");

                connection2.Open();
                string s1 = @"SELECT dbo.Trafic.Nume_Traf, dbo.Sectoare.Nume_Sec, dbo.Sectoare.GradPericol_Sec, dbo.Trafic.Permis_Traf
                FROM dbo.Sectoare INNER JOI  dbo.Trafic ON dbo.Sectoare.ID_Sec = dbo.Trafic.ID_Sec WHERE Nume_Traf='" + Nume_Traf.Text + "'";
                SqlCommand cmd1 = new SqlCommand(s1, connection2);
                SqlDataReader dr1 = cmd1.ExecuteReader();

                while (dr1.Read())
                {
                   Nume_Traf.Text = (dr1["Nume_Traf"].ToString());
                   
                }
                dr1.Close();

                connection2.Close();
                MessageBox.Show("Confirmă ștergerea");

            }
        }

        private async void Confirma2_click(object sender, RoutedEventArgs e)
        {
            SqlConnection connection2 = new SqlConnection(@"Data Source = .\BLUECC; Initial Catalog = Drones; Integrated Security=True");

            string s1 = @"Delete  from Trafic where Nume_Traf='" + Nume_Traf.Text + "'";
            connection2.Open();
            SqlCommand cmd2 = new SqlCommand(s1, connection2);

            cmd2.ExecuteNonQuery();
            connection2.Close();
            var metroWindow = (Application.Current.MainWindow as MetroWindow);
            await metroWindow.ShowMessageAsync("Atenție", " Proprietar șters");
        }
        private bool CheckFields()
        {
            bool ok = true;

            if (Nume_Traf.Text == "")
            {
                ok = false;
                Nume_Traf.BorderBrush = new SolidColorBrush(Colors.Red);
            }

            return ok;
        }
    }

}

