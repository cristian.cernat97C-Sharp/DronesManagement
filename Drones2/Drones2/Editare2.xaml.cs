﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data.SqlClient;
using System.Data;
using System.ComponentModel;
namespace Drones2
{
    /// <summary>
    /// Interaction logic for Editeaza.xaml
    /// </summary>
    public partial class Editare2 : UserControl
    {
        bool exists = false;

        public Editare2()
        {
            InitializeComponent();
            FillData();
          
        }

        private int SectorStuff()
        {
            int Sec = 1;
            string connectionString = @"Data Source=.\BLUECC;Initial Catalog=Drones;Integrated Security=True";
            SqlConnection connection = new SqlConnection(connectionString);

            string local = "SELECT ID_Sec FROM [Sectoare] WHERE Nume_Sec = @Nume_Sec AND GradPericol_Sec=@GradPericol_Sec ";
            SqlCommand cmd = new SqlCommand(local, connection);
            connection.Open();
            cmd.Parameters.AddWithValue("@Nume_Sec", NumeSector.Text);
            cmd.Parameters.AddWithValue("@GradPericol_Sec", GradPericol.Text);
            try
            {
                Sec = (int)(cmd.ExecuteScalar());
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }

            connection.Close();
            return Sec;
        }
        private void Edit_Save(object sender, RoutedEventArgs e)
        {
            Class1.connection.Close();

            //Views.CustomFlyout ob = new CustomFlyout();

            bool chk_result = CheckFields();
            //string sex = ob.CheckSex();
            //string buget = ob.CheckBuget();


            if (chk_result == true)
            {
                Class1.connection.Open();

                using (SqlCommand cmd = new SqlCommand("SELECT COUNT(*) FROM [Trafic] WHERE Nume_Traf = @Nume_Traf ", Class1.connection))
                {
                    cmd.Parameters.AddWithValue("Nume_Traf", Nume_Traf.Text);
                    exists = (int)cmd.ExecuteScalar() > 0;
                }

                if (exists == false)
                {
                    MessageBox.Show("Nu e asa obiect");
                }
                else
                {
                    using (SqlCommand cmd = new SqlCommand("UPDATE [Trafic] SET Nume_Traf=@Nume_Traf, ID_Sec=@ID_Sec,Permis_Traf=@Permis_Traf WHERE Nume_Traf=@Nume_Traf", Class1.connection))
                    {
                        cmd.Parameters.AddWithValue("Nume_Traf", Nume_Traf.Text);
                        cmd.Parameters.AddWithValue("ID_Sec", SectorStuff());
                        cmd.Parameters.AddWithValue("Permis_Traf", PermisTraf.Text);
                    
    
                        cmd.ExecuteNonQuery();

                        MessageBox.Show("Realizat");
                        Class1.connection.Close();
                    }
                }

                if (Class1.connection.State == ConnectionState.Open)
                {
                    Class1.connection.Close();
                }
            }
            Class1.connection.Close();
        }
        
        void FillData()
        {
            if (Class1.connection.State == ConnectionState.Open)
            {
                Class1.connection.Close();
            }

            using (Class1.connection)
            {
                Class1.connection.Open();

                using (SqlDataAdapter dataAdapter = new SqlDataAdapter(@"SELECT dbo.Trafic.Nume_Traf, dbo.Sectoare.Nume_Sec, dbo.Sectoare.GradPericol_Sec, dbo.Trafic.Permis_Traf FROM
                  dbo.Sectoare INNER JOIN dbo.Trafic ON dbo.Sectoare.ID_Sec = dbo.Trafic.ID_Sec", Class1.connection))
                {
                    DataTable dt = new DataTable();
                    dataAdapter.Fill(dt);

                    TrafGrid.ItemsSource = dt.DefaultView;

                }
            }
        }


        private bool CheckFields()
        {
            bool ok = true;

            if (Nume_Traf.Text == "")
            {
                ok = false;
                Nume_Traf.BorderBrush = new SolidColorBrush(Colors.Red);
            }
            else
            if (NumeSector.Text == "")
            {
                ok = false;
                NumeSector.BorderBrush = new SolidColorBrush(Colors.Red);
            }
            else
            if (GradPericol.Text == "")
            {
                ok = false;
                GradPericol.BorderBrush = new SolidColorBrush(Colors.Red);
            }
            else
            if (PermisTraf.Text == "")
            {
                ok = false;
                PermisTraf.BorderBrush = new SolidColorBrush(Colors.Red);
            }

            return ok;
        }
    }
}
