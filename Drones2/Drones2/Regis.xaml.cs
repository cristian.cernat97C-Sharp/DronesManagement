﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data.SqlClient;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using System.Data;

namespace Drones2
{
    /// <summary>
    /// Interaction logic for CustomFlyout.xaml
    /// </summary>
    public partial class RegisFlyout : UserControl
    {
        String sqlStatement = "SELECT TOP 1 ID_Prop FROM [Proprietari] ORDER BY ID_Prop DESC";
        bool exists = false;
        int studCounter = 0;

        public RegisFlyout()
        {
            InitializeComponent();
            SetDatePicker();
            
        }

        public void Curatenie()
        {
            Login.Clear();
            Parola.Clear();
            Nume.Clear();
            Prenume.Clear();
            Email.Clear();
            SetDatePicker();
            Adresa.Clear();
            Telefonul.Clear();
            SexFem.IsChecked = true;
        }


        private async void Create_Prop(object sender, RoutedEventArgs e)
        {
            
            


            bool chk_result = CheckFields();

            if (chk_result == true)
            {
                Class1.connection.Open();

                using (SqlCommand cmdCount = new SqlCommand(sqlStatement, Class1.connection))
                {
                    studCounter = (int)cmdCount.ExecuteScalar() + 1;
                }

                using (SqlCommand cmd = new SqlCommand("SELECT COUNT(*) FROM [Proprietari] WHERE Login1 = @Login1", Class1.connection))
                {
                    cmd.Parameters.AddWithValue("Login1", Login.Text);
                    exists = (int)cmd.ExecuteScalar() > 0;
                }

                if (exists)
                {
                    var metroWindow = (Application.Current.MainWindow as MetroWindow);
                    await metroWindow.ShowMessageAsync("Exista", "Asa Proprietar");
                }
                else
                {
                    using (SqlCommand cmd = new SqlCommand("INSERT INTO [Proprietari] VALUES (@ID_Prop, @Login1, @Password1, @Email, @Nume_Prop, @Prenume_Prop, @Gen_Prop, @DatNas_Prop, @Adresa_Prop, @Tel_Prop)", Class1.connection))
                    {
                        cmd.Parameters.AddWithValue("ID_Prop", studCounter);
                        cmd.Parameters.AddWithValue("Login1", Login.Text);
                        cmd.Parameters.AddWithValue("Password1", Parola.Password);
                        cmd.Parameters.AddWithValue("Email", Email.Text);
                        cmd.Parameters.AddWithValue("Nume_Prop", Nume.Text);
                        cmd.Parameters.AddWithValue("Prenume_Prop", Prenume.Text);
                        cmd.Parameters.AddWithValue("Gen_Prop", CheckSex());
                        cmd.Parameters.AddWithValue("DatNas_Prop", DataNastere.SelectedDate.Value.Date.ToString("yyyy-MM-dd"));
                        cmd.Parameters.AddWithValue("Adresa_Prop", Adresa.Text);
                        cmd.Parameters.AddWithValue("Tel_Prop", Telefonul.Text);

                        cmd.ExecuteNonQuery();

                        var metroWindow = (Application.Current.MainWindow as MetroWindow);
                        await metroWindow.ShowMessageAsync("Înregistrare", "Succes");
                        Curatenie();
                        Class1.connection.Close();
                        
                    }

                }

                if (Class1.connection.State == ConnectionState.Open)
                {
                    Class1.connection.Close();
                }
            }
        }
        private string CheckSex()
        {
            string gen = "";
            if (SexFem.IsChecked == true) gen = "F";
            else gen = "M";

            return gen;
        }

        private void SetDatePicker()
        {
            DataNastere.SelectedDate = new DateTime(2000, 1, 1);
            DataNastere.DisplayDateStart = new DateTime(1970, 1, 1);
            DataNastere.DisplayDateEnd = new DateTime(2005, 1, 1);
            DataNastere.DisplayDate = new DateTime(2000, 1, 1);
        }

        private bool CheckFields()
        {
            bool ok = true;

            if (Login.Text == "")
            {
                ok = false;
                Login.BorderBrush = new SolidColorBrush(Colors.Red);
            }else
            if (Parola.Password == "")
            {
                ok = false;
                Parola.BorderBrush = new SolidColorBrush(Colors.Red);
            }else
            if (Email.Text == "")
            {
                ok = false;
                Email.BorderBrush = new SolidColorBrush(Colors.Red); 
            }else
            if (Nume.Text == "")
            {
                ok = false;
                Nume.BorderBrush = new SolidColorBrush(Colors.Red);
            }
            else if (Prenume.Text == "")
            {
                ok = false;
                Prenume.BorderBrush = new SolidColorBrush(Colors.Red);
            }
            
            else if (DataNastere.Text == "")
            {
                ok = false;
                DataNastere.BorderBrush = new SolidColorBrush(Colors.Red);
            }
            else if (Adresa.Text == "")
            {
                ok = false;
                Adresa.BorderBrush = new SolidColorBrush(Colors.Red);
            }
            else if (Telefonul.Text == "")
            {
                ok = false;
                Telefonul.BorderBrush = new SolidColorBrush(Colors.Red);
            }

            return ok;
        }

        private async Task SuccessMessage()
        {
            MainWindow mw = new MainWindow();
            await mw.ShowMessageAsync("Success", "Successfuly registered!");
        }

      
    }
}
