# Project Title

MahApps libarary installed with Nunugget was used for this project in order to use the metro style from Windows 8

![Screenshot_10](/uploads/16403df362209879cdd3168542da6881/Screenshot_10.jpg)
![Screenshot_12](/uploads/0dd5f76cb7c5b3ce591e925bbe1f252a/Screenshot_12.jpg)
![Screenshot_11](/uploads/75917ecdbf8c4038b6ebd11f771c0c6f/Screenshot_11.jpg)
![Screenshot_13](/uploads/7661b49f3167de6ba9c0c1084577c4c8/Screenshot_13.jpg)
![Screenshot_14](/uploads/2a71c298c0f0314ff26925f6afc2b9ee/Screenshot_14.jpg)
![Screenshot_15](/uploads/29e9360ae8e0938ac1721e41dacb3833/Screenshot_15.jpg)
![Screenshot_16](/uploads/4667919106549657769935146940440c/Screenshot_16.jpg)


## Getting Started
Change the Class1.cs and other connection from files to your database

Run Drones.exe shourtcut from Drones2/bin/debug

### Prerequisites

You'll need to run the sql file to get the database in place
Change the Class1.cs and other connection from files to your database
````@"Data Source = localhost; Initial Catalog = Drones; Integrated Security=True"```` 
or
````@"Data Source = .\BLUECC; Initial Catalog = Drones; Integrated Security=True"```` 
where BLUECC is the name of the instance


### Installing

Using git

```
 git clone https://gitlab.com/cristian.cernat97C-Sharp/DronesManagement.git
```

## Built With

* [MahhApps](http://mahapps.com) - MetroStyle

## Contributing

Go ahead blame me for anything I am still learning 
## Versioning

Version 1.0 

## Authors

* **Cristian Cernat** - (https://gitlab.com/cristian.cernat97)