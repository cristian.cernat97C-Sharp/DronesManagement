﻿using System;
using System.Net.NetworkInformation;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using Microsoft.Maps.MapControl.WPF;

namespace Drones2
{
    public class MapContainer : Grid
    {
        public Action<Map> MapLoaded;

        #region Private Properties

        private Map _map;
        private bool _mapLoaded;

        #endregion

        #region Constructor

        public MapContainer()
        {
            this.Loaded += (s, e) =>
            {
                if (NetworkInterface.GetIsNetworkAvailable())
                {
                    _map = new Map();
                    this.Children.Add(_map);

                    _mapLoaded = true;

                    if (MapLoaded != null)
                    {
                        MapLoaded(_map);
                    }
                }
                else
                {
                    NetworkChange.NetworkAvailabilityChanged += new NetworkAvailabilityChangedEventHandler(NetworkChange_NetworkAvailabilityChanged);

                    this.Children.Add(new Grid()
                    {
                        Background = new SolidColorBrush(Colors.Transparent),
                        Children =
                        {
                            //Optional Background image of a map.
                            new Image()
                            {
                                Source = new BitmapImage(new Uri("pack://application:,,,/BackgroundMap.png", UriKind.RelativeOrAbsolute))
                            },

                            //Error Message due to lack of internet connection
                            new Border(){
                                Background = new SolidColorBrush(Colors.White),
                                CornerRadius = new System.Windows.CornerRadius(10),
                                Padding = new Thickness(10),
                                HorizontalAlignment = System.Windows.HorizontalAlignment.Center,
                                VerticalAlignment = System.Windows.VerticalAlignment.Center,
                                Child = new TextBlock()
                                {
                                    Text = "Unable to load map without Internet Connection.",
                                    HorizontalAlignment = System.Windows.HorizontalAlignment.Center,
                                    VerticalAlignment = System.Windows.VerticalAlignment.Center
                                }
                            }
                        }
                    });
                }
            };
        }

        #endregion

        #region Public Methods

        public Map Map
        {
            get { return _map; }
        }

        public bool IsMapLoaded
        {
            get { return _mapLoaded; }
        }

        #endregion

        #region Private Methods

        private void NetworkChange_NetworkAvailabilityChanged(object sender, NetworkAvailabilityEventArgs args)
        {
            if (args.IsAvailable)
            {
                this.Dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action(delegate
                {
                    this.Children.Clear();

                    _map = new Map();
                    this.Children.Add(_map);

                    _mapLoaded = true;

                    if (MapLoaded != null)
                    {
                        MapLoaded(_map);
                    }

                    NetworkChange.NetworkAvailabilityChanged -= NetworkChange_NetworkAvailabilityChanged;
                }));
            }
        }

        #endregion
    }
}