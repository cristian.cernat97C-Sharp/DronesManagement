﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data.SqlClient;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using System.Data;
namespace Drones2
{
    /// <summary>
    /// Interaction logic for DeleteElevFlyout.xaml
    /// </summary>
    public partial class DeleteDroneFlyout : UserControl
    {
        public DeleteDroneFlyout()
        {
            InitializeComponent();
        }

        public void Curatenie()
        {
            
            Nume.Clear();
            Putere.Clear();


        }

        private async void Sterge_dron_click(object sender, RoutedEventArgs e)
        {
            bool chk_result = CheckFields();

            if (chk_result == true)
            {
                SqlConnection connection2 = new SqlConnection(@"Data Source = .\BLUECC; Initial Catalog = Drones; Integrated Security=True");

                connection2.Open();
                string s1 = @"Select Putere_Dron from Drone where Nume_Dron='" + Putere.Text + "'";
                SqlCommand cmd1 = new SqlCommand(s1, connection2);
                SqlDataReader dr1 = cmd1.ExecuteReader();

                while (dr1.Read())
                {
                    Putere.Text = (dr1["Nume_Prop"].ToString());
                    

                }
                dr1.Close();

                connection2.Close();
                MessageBox.Show("Confirmă ștergerea");


            }
        }

        private async void Confirma_dron_click(object sender, RoutedEventArgs e)
        {
            SqlConnection connection2 = new SqlConnection(@"Data Source = .\BLUECC; Initial Catalog = Drones; Integrated Security=True");

            string s1 = @"Delete  from Drone where Nume_Dron='" + Nume.Text + "'";
            connection2.Open();
            SqlCommand cmd2 = new SqlCommand(s1, connection2);

            cmd2.ExecuteNonQuery();
            connection2.Close();
            var metroWindow = (Application.Current.MainWindow as MetroWindow);
            await metroWindow.ShowMessageAsync("Atenție", " Proprietar șters");
        }
        private bool CheckFields()
        {
            bool ok = true;

            if (Nume.Text == "")
            {
                ok = false;
                Nume.BorderBrush = new SolidColorBrush(Colors.Red);
            }


            return ok;
        }
    }

}

