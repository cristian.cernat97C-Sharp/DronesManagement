﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using System.Data.SqlClient;

namespace Drones2
{
    /// <summary>
    /// Interaction logic for Welcome.xaml
    /// </summary>
    public partial class Welcome : MetroWindow
    {
        public Welcome()
        {
            InitializeComponent();
        }
        public bool IsOp = false;


        public void Disable(int a)
        {
            if (a == 1) { }

        }
        public void ToggleFlyout(int index)
        {

            var thisflyout = this.Flyouts.Items[index] as Flyout;

            for (int i = 0; i <= 1; i++)
            {
                if (i == index) continue;
                var flyout = this.Flyouts.Items[i] as Flyout;
                if (flyout.IsOpen == true) { flyout.IsOpen = false; }
            }

            thisflyout.IsOpen = !thisflyout.IsOpen;
        }
        
        public void Regis_click(object sender, RoutedEventArgs e)
        {
            ToggleFlyout(0);
            RegisFlyout ob = new RegisFlyout();
            ob.Curatenie();
           
        }
        public void Login_click(object sender, RoutedEventArgs e)
        {
            ToggleFlyout(1);
            LoginFlyout ob = new LoginFlyout();
            ob.Curatenie();
            


        }

    }
}
