﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data.SqlClient;
using MahApps.Metro.Controls.Dialogs;
using System.Data;
using MahApps.Metro.Controls;

namespace Drones2
{
    /// <summary>
    /// Interaction logic for CustomFlyout.xaml
    /// </summary>
    public partial class AddCollisionFlyout : UserControl
    {
        String sqlStatement = "SELECT TOP 1 ID_Acc FROM [Accidente] ORDER BY ID_Acc DESC";
        bool exists = false;
        int studCounter = 0;
        

        public AddCollisionFlyout()
        {
            InitializeComponent();
            SetDatePicker();

        }

        public void Curatenie()
        {
            NumeAcc.Clear();
            Circum.Clear();
            Rezultat.Clear();
            SetDatePicker();
            
        }


        private async void Create_Collision(object sender, RoutedEventArgs e)
        {
            string connectionString = @"Data Source=.\BLUECC;Initial Catalog=Drones;Integrated Security=True";
            SqlConnection connection = new SqlConnection(connectionString);


            int trafic = GetTraffic();
            bool chk_result = CheckFields();

            if (chk_result == true)
            {
                Class1.connection.Open();

                using (SqlCommand cmdCount = new SqlCommand(sqlStatement, connection))
                {
                    studCounter = (int)cmdCount.ExecuteScalar() + 1;
                }

                using (SqlCommand cmd = new SqlCommand("SELECT COUNT(*) FROM [Accidente] WHERE Nume_Acc = @Nume_Acc", connection))
                {
                    cmd.Parameters.AddWithValue("Nume_Acc", NumeAcc.Text);
                    exists = (int)cmd.ExecuteScalar() > 0;
                }

                if (exists)
                {
                    MessageBox.Show("Accident already exists!");
                }
                else
                {
                    using (SqlCommand cmd = new SqlCommand("INSERT INTO [Accidente] VALUES (@ID_Acc, @Nume_Acc, @Data_Acc, @Circum_Acc, @Rezult_Acc, @ID_Traf)", connection))
                    {
                        cmd.Parameters.AddWithValue("ID_Acc", studCounter);
                        cmd.Parameters.AddWithValue("Nume_Acc", NumeAcc.Text);
                        cmd.Parameters.AddWithValue("Data_Acc", Data.SelectedDate.Value.Date.ToString("yyyy-MM-dd"));
                        cmd.Parameters.AddWithValue("Circum_Acc", Circum.Text);
                        cmd.Parameters.AddWithValue("Rezult_Acc", Rezultat.Text);
                        cmd.Parameters.AddWithValue("ID_Traf",trafic);

                        cmd.ExecuteNonQuery();

                        var metroWindow = (Application.Current.MainWindow as MetroWindow);
                        await metroWindow.ShowMessageAsync("Operațiune", "Cu succes");

                        connection.Close();
                    }
                }

                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
        }

        public int GetTraffic()
        {
            int loc = 1;
            string connectionString = @"Data Source=.\BLUECC;Initial Catalog=Drones;Integrated Security=True";
            SqlConnection connection = new SqlConnection(connectionString);

            string local = "SELECT ID_Traf FROM [Trafic] WHERE Nume_Traf = @Nume_Traf";
            SqlCommand cmd = new SqlCommand(local, connection);
            connection.Open();
            cmd.Parameters.AddWithValue("@Nume_Traf", NumeTraf.Text);
            loc = (int)(cmd.ExecuteScalar());

            connection.Close();
            return loc;
        }

      

        public void SetDatePicker()
        {
            Data.SelectedDate = new DateTime(2000, 1, 1);
            Data.DisplayDateStart = new DateTime(1970, 1, 1);
            Data.DisplayDateEnd = new DateTime(2005, 1, 1);
            Data.DisplayDate = new DateTime(2000, 1, 1);
        }

        public bool CheckFields()
        {
            bool ok = true;


            if (NumeAcc.Text == "")
            {
                ok = false;
                NumeAcc.BorderBrush = new SolidColorBrush(Colors.Red);
            }
            else if (Circum.Text == "")
            {
                ok = false;
                Circum.BorderBrush = new SolidColorBrush(Colors.Red);
            }
            else if (Rezultat.Text == "")
            {
                ok = false;
                Rezultat.BorderBrush = new SolidColorBrush(Colors.Red);
            }
            else if (NumeTraf.Text == "")
            {
                ok = false;
                NumeTraf.BorderBrush = new SolidColorBrush(Colors.Red);
            }
           

            return ok;
        }

        

    }
}
